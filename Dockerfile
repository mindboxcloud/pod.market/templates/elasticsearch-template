FROM registry.access.redhat.com/rhel7.7:latest

MAINTAINER R&D Mindbox <automation.pod.market@mindboxgroup.com>
LABEL io.k8s.description="search and analytics engine" \
      io.k8s.display-name="Elasticsearch" \
      io.openshift.expose-services="9200:http 9300:http" \
      io.openshift.tags="elasticsearch"

ENV ELASTICSEARCH_VERSION=7.4.1
ENV OPENJDK_VERSION=11
ENV USER_NAME=elasticsearch USER=elasticsearch LOGNAME=elasticsearch
# Elasticsearch install
RUN yum update -y && \
    yum -y install perl-Digest-SHA java-${OPENJDK_VERSION}-openjdk && \
    curl -O https://artifacts.elastic.co/downloads/elasticsearch/elasticsearch-${ELASTICSEARCH_VERSION}-x86_64.rpm  && \
    curl -O https://artifacts.elastic.co/downloads/elasticsearch/elasticsearch-${ELASTICSEARCH_VERSION}-x86_64.rpm.sha512  && \
    shasum -a 512 -c elasticsearch-${ELASTICSEARCH_VERSION}-x86_64.rpm.sha512 && \
    rpm --install elasticsearch-${ELASTICSEARCH_VERSION}-x86_64.rpm && \
    rm -f elasticsearch-${ELASTICSEARCH_VERSION}-x86_64.rpm.sha512 && \
    rm -f elasticsearch-${ELASTICSEARCH_VERSION}-x86_64.rpm && \
    yum clean all -y && \
    rm -rf /var/cache/yum

# Elasticsearch setup
WORKDIR /usr/share/elasticsearch
ENV LANG=en_US.UTF8
ENV HOME=/usr/share/elasticsearch
RUN usermod -u 1001 -c "elasticsearch" -d /usr/share/elasticsearch -g 0 elasticsearch && \
    mkdir -p /usr/share/elasticsearch/config  && \
    chmod -R u+x /usr/share/elasticsearch /etc/sysconfig/elasticsearch /etc/elasticsearch /var/log/elasticsearch /var/lib/elasticsearch && \
    chgrp -R 0 /usr/share/elasticsearch /etc/sysconfig/elasticsearch /etc/elasticsearch /var/log/elasticsearch /var/lib/elasticsearch && \
    chmod -R g=u /usr/share/elasticsearch /etc/sysconfig/elasticsearch /etc/elasticsearch /var/log/elasticsearch /var/lib/elasticsearch


COPY jvm.options /etc/elasticsearch/jvm.options

# Expose ports
EXPOSE 9200 9300
USER 1001
ENTRYPOINT ["/usr/share/elasticsearch/bin/elasticsearch"]

# openshift-elasticsearch    
# elasticsearch java configuration    
Java parameters can be changed by changing env variable
```
ES_JAVA_OPTS="-Xms2g -Xmx2g -XX:+UseG1GC"
```

# 3 node cluster    
set replicas to 3
```
replicas: 3
```

and env cluster.initial_master_nodes
```
dc-${INSTANCE_NAME}-0,dc-${INSTANCE_NAME}-1,dc-${INSTANCE_NAME}-2
```
